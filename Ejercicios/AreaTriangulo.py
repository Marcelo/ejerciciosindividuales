# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Geometría Computacional
# Cálculo del área de un triángulo
# Descripción: Usando el área del triángulo se define si un punto está a la izquierda o derecha de una recta dada

from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback
if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
import math
import numpy as np

pygame.display.init()
pygame.font.init()
#Screen configuration
screen_size = [800,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Recta y Punto")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)
pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

print(glGetIntegerv(GL_MAX_TEXTURE_SIZE))

glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [90.0,0.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0] #The sphere's center
In_X = 0
In_Y = 0

def get_input():
    global camera_rot, camera_radius, In_Y, In_X
    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()
    mouse_rel = pygame.mouse.get_rel()
    for event in pygame.event.get():
        if   event.type == QUIT: return False
        elif event.type == KEYDOWN:
            if   event.key == K_ESCAPE: return False
        elif event.type == MOUSEBUTTONDOWN:
            #Zoom in
            if   event.button == 4: camera_radius *= 0.9
            #Or out.
            elif event.button == 5: camera_radius /= 0.9
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    if sum(mouse_position) > 0:
        posX, posY = pygame.mouse.get_pos()
        In_X, In_Y = (posX-400)/10, (-posY+300)/10
    return True

def draw():
    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glViewport(0,0,screen_size[0],screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0])/float(screen_size[1]), 0.1,100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius*cos(radians(camera_rot[0]))*cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius                            *sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius*sin(radians(camera_rot[0]))*cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0],camera_pos[1],camera_pos[2],
        camera_center[0],camera_center[1],camera_center[2],
        0,1,0
    )

    #Se define la ecuación y= mx + b
    # y = 3x - 2
    #Se definen los puntos
    #Pto 1
    ay_1 = -6.0
    ax_1 = (ay_1 + 2.0)/3.0
    # Pto 2
    ay_2 = 6.0
    ax_2 = (ay_2 + 2.0) / 3.0

    #Se grafica la recta
    glColor3f(1,0,0)
    #Start drawing triangles.  Each subsequent triplet of glVertex*() calls will draw one triangle.
    glBegin(GL_LINES)
    glColor3f(1, 0, 0)
    glVertex3f(ax_1,ay_1,0.0) #Make a vertex at (0.0,0.0,0.0)
    #glColor3f(0, 0, 1)
    glVertex3f(ax_2,ay_2,0.0) #Make a vertex at (0.8,0.0,0.0)
    glEnd()

    #Se dibuja el punto
    glPushMatrix()
    glTranslatef(In_X,In_Y,0)
    glColor3f(0, 0, 1)
    glBegin(GL_POLYGON)
    for i in np.arange(0.0, 10.0, 0.1):
        x = 0.05 * math.cos(i)
        y = 0.05 * math.sin(i)
        glVertex3f(x, y, 0)
    glEnd()
    glPopMatrix()

    #Se realizan los cálculos para saber de qué lado está el punto
    R = 0.5*(ax_1*(ay_2-In_Y) + ax_2*(In_Y-ay_1) + In_X*(ay_1-ay_2))

    if (R < 0.0):
        print("Está a la derecha de la recta")
        # Se dibuja el punto
        glPushMatrix()
        glTranslatef(In_X, In_Y, 0)
        glColor3f(1, 1, 0)
        glBegin(GL_POLYGON)
        for i in np.arange(0.0, 10.0, 0.1):
            x = 0.5 * math.cos(i)
            y = 0.5 * math.sin(i)
            glVertex3f(x, y, 0)
        glEnd()
        glPopMatrix()
    if (R > 0.0):
        print("Está a la izquierda de la recta")
        # Se dibuja el punto
        glPushMatrix()
        glTranslatef(In_X, In_Y, 0)
        glColor3f(1, 0, 1)
        glBegin(GL_POLYGON)
        for i in np.arange(0.0, 10.0, 0.1):
            x = 0.5 * math.cos(i)
            y = 0.5 * math.sin(i)
            glVertex3f(x, y, 0)
        glEnd()
        glPopMatrix()
    if (R == 0.0):
        print("Está sobre la recta")
    #print('X:' + str(In_X) + " Y:" + str(In_Y))
    pygame.display.flip()

def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw()
        clock.tick(60) #Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()

if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()