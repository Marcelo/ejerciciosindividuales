# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Geometría Computacional
# Distancia Circunferencia Mínima
# Descripción: A partir de un conjunto de puntos, calcular cual es la circunferencia mínima

from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback

from pyglet.gl import glPointSize

if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
import math
import numpy as np

pygame.display.init()
pygame.font.init()
#Screen configuration
screen_size = [800,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Circunferencia Mínima")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)
pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

print(glGetIntegerv(GL_MAX_TEXTURE_SIZE))

glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [90.0,0.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0] #The sphere's center
In_X = 0
In_Y = 0
S = []
cont=0
A = []
B = []
#Genera un conjunto de puntos aleatorios
S = np.array([(np.random.randint(-300, 300), np.random.randint(-300, 300)) for i in range(50)])

def get_input():
    global camera_rot, camera_radius, In_Y, In_X, S, cont
    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()
    mouse_rel = pygame.mouse.get_rel()

    for event in pygame.event.get():
        if   event.type == QUIT: return False
        elif event.type == KEYDOWN:
            if   event.key == K_ESCAPE: return False
        elif event.type == MOUSEBUTTONDOWN:
            #Zoom in
            if   event.button == 4: camera_radius *= 0.9
            #Or out.
            elif event.button == 5: camera_radius /= 0.9
    if mouse_buttons[0]:
        camera_center[0] += mouse_rel[0]/5
        camera_center[1] -= mouse_rel[1]/5

    #mueve un puntero sobre la pantalla
    if sum(mouse_position) > 0:
        posX, posY = pygame.mouse.get_pos()
        In_X, In_Y = (posX-400)/20, (-posY+300)/20

    return True

#Devuelve el punto medio entre dos puntos dados
def ptoMedio(ax, ay, bx, by):
    cx = (ax + bx) / 2
    cy = (ay + by) / 2
    return cx, cy

#Calcula la distancia entre dos puntos
def dist(ax, ay, bx, by):
    x = (bx - ax)**2
    y = (by - ay) ** 2
    R = math.sqrt(x+y)
    return R

#Devuelve el centro y radio a partir de tres puntos
def centroRadio(ax, ay, bx, by, cx, cy):
    A = np.asmatrix([[ax, ay, 1], [bx, by, 1], [cx, cy, 1]])
    b = np.asmatrix([[-(ax**2 + ay**2)], [-(bx**2 + by**2)], [-(cx**2 + cy**2)]])
    D, E, F = (A ** -1) * b
    h = float(-D / 2)
    k = float(-E / 2)
    r = math.sqrt(h**2 + k**2 - F)
    return h, k, r

#Dibuja los puntos
def draw():
    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glViewport(0,0,screen_size[0],screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0])/float(screen_size[1]), 0.1,100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius*cos(radians(camera_rot[0]))*cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius                            *sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius*sin(radians(camera_rot[0]))*cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0],camera_pos[1],camera_pos[2],
        camera_center[0],camera_center[1],camera_center[2],
        0,1,0
    )

    #Se dibuja el puntero
    glPushMatrix()
    glColor3f(1, 0, 1)
    glPointSize(5)
    glBegin(GL_POINTS)
    glVertex3f(In_X,In_Y,0)
    glEnd()
    glPopMatrix()

    # Se dibujan los puntos seleccionados
    glColor3f(1, 0, 0)
    for i in range(len(S)):
        #Se redefine la posición de los puntos
        posX, posY= S[i]
        px, py = (posX) / 20, (posY) / 20
        glPushMatrix()
        glPointSize(5)
        glBegin(GL_POINTS)
        glVertex3f(px, py, 0)
        glEnd()
        glPopMatrix()

    #Se procede a realizar el algoritmo que calcule las distancias
    pygame.event.pump()
    key = pygame.key.get_pressed()

    for i in range(len(S)):
        for j in range(len(S)):
            sum = 0
            if j != i:
                ax, ay = S[i]
                bx, by = S[j]
                R1 = dist(ax, ay, bx, by) / 2
                cx, cy = ptoMedio(ax, ay, bx, by)
                for k in range(len(S)):
                    dx, dy = S[k]
                    R2 = dist(cx, cy, dx, dy)
                    if (R1 >= R2):
                        sum += 1
                    else:
                        break
            # Verifica si cumple la condición y dibuja la circunferencia
            if sum == (len(S)):
                ax, ay = S[i]
                bx, by = S[j]
                R1 = dist(ax, ay, bx, by) / 2
                cx, cy = ptoMedio(ax, ay, bx, by)
                cx, cy = (cx) / 20, (cy) / 20
                glColor3f(1, 1, 0)
                glPushMatrix()
                glTranslatef(cx, cy, 0)
                glBegin(GL_POLYGON)
                for i in np.arange(0.0, 10.0, 0.1):
                    x = R1/20 * math.cos(i)
                    y = R1/20 * math.sin(i)
                    glVertex3f(x, y, 0)
                glEnd()
                glPopMatrix()
                break

    """aux = 0
    sum = 0
    for i in range(len(S)):
        for j in range(len(S)):
            if j != i:
                sum = 0
                for k in range(len(S)):
                    if (k != j) and (k != i):
                        ax, ay = S[i]
                        bx, by = S[j]
                        cx, cy = S[k]
                        m, n, r = centroRadio(ax, ay, bx, by, cx, cy)

                        for l in range(len(S)):
                            dx, dy = S[l]
                            R2 = dist(m, n, dx, dy)
                            if (r >= R2):
                                sum += 1
                                if (sum == len(S)):
                                    aux = k
                                    break
                            else:
                                break

            # Verifica si cumple la condición y dibuja la circunferencia
            if sum == (len(S)):
                ax, ay = S[i]
                bx, by = S[j]
                cx, cy = S[aux]
                ax, ay = (ax) / 20, (ay) / 20
                bx, by = (bx) / 20, (by) / 20
                cx, cy = (cx) / 20, (cy) / 20
                h, k, r = centroRadio(ax, ay, bx, by, cx, cy)

                glColor3f(0, 1, 1)
                glPushMatrix()
                glTranslatef(h, k, 0)
                glBegin(GL_POLYGON)
                for i in np.arange(0.0, 10.0, 0.1):
                    x = r * math.cos(i)
                    y = r * math.sin(i)
                    glVertex3f(x, y, 0)
                glEnd()
                glPopMatrix()
                break"""

    pygame.display.flip()

def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw()
        clock.tick(60) #Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()

if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()