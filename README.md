# EjerciciosIndividuales

Este programa contiene 3 archivos, los cuales se describen a continuación:

1. Algoritmo que encuentra la circunferencia mínima de un conjunto de puntos generados de forma aleatoria.

    ![imagen1](https://framagit.org/Marcelo/ejerciciosindividuales/-/raw/main/Ejercicios/Im%C3%A1genes/CircunferenciaMinima.png)

2. Algoritmo que calcula la distancia mínima y máxima de un conjunto de puntos.

    ![imagen1](https://framagit.org/Marcelo/ejerciciosindividuales/-/raw/main/Ejercicios/Im%C3%A1genes/DistanciaMinMax.png)

3. Algoritmo que calcula la posición de un punto respecto de una recta mediante Signalidad.
   Se muestra el resultado cuando el punto esta a la izquierda y a la derecha

    ![imagen1](https://framagit.org/Marcelo/ejerciciosindividuales/-/raw/main/Ejercicios/Im%C3%A1genes/SignalidadIzq.png)
    ![imagen1](https://framagit.org/Marcelo/ejerciciosindividuales/-/raw/main/Ejercicios/Im%C3%A1genes/SignalidadDer.png)
